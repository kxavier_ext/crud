// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase : {
    apiKey: "AIzaSyD7oommnOloZRKzFl3PKdJ-DHrBfQBmIkE",
    authDomain: "whitelabelplatform.firebaseapp.com",
    databaseURL: "https://whitelabelplatform.firebaseio.com",
    projectId: "whitelabelplatform",
    storageBucket: "whitelabelplatform.appspot.com",
    messagingSenderId: "858648870648",
    appId: "1:858648870648:web:8729c39ebd06bcb989050d"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.



